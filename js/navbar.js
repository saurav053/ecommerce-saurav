var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var B= document.body; //IE 'quirks'
var D= document.documentElement; //IE with doctype
D= (D.clientHeight)? D: B;

var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
    if (D.scrollTop == 0) {
        document.getElementById("title-bar").style.display='block'
        document.getElementById("navbar").style.top = "60px";
        document.getElementById("navbar").classList.remove("nav-shadow");
    }    
    else{
        document.getElementById("navbar").style.top = "0px";
        document.getElementById("navbar").classList.add("nav-shadow");
    } 
    } else {
    document.getElementById("title-bar").style.display='none'
    document.getElementById("navbar").style.top = "-60px";
    }
    prevScrollpos = currentScrollPos;
}